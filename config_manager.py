from db.models import Configuration
from bitbucket.models import Repository


def shas_team_id():
    return Configuration.by_name('bitbucket_shas_team_id').value


def bitbucket_user_id(user=''):
    if user is not '':
        return Configuration.by_name('bitbucket_{user}_id'.format(user=user)).value
    else:
        return ''


def bot_token():
    return Configuration.by_name('bot_token').value


def bitbucket_token():
    return Configuration.by_name('bitbucket_token').value


def repository_id(repo):
    return Repository.by_name('{repo}'.format(repo=repo.lower())).uuid
