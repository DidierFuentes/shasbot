import json

from flask import Flask, request

import config_manager as cm
import Logger
import telegram as t

from bitbucket.client import BitbucketClient
from bitbucket.parser import BitbucketParser
from bitbucket.models import Issue
from db.database import db_session
from db.models import User, Conversation, Command

app = Flask(__name__)
bot_token = cm.bot_token()

# Available commands
START = '/start'
VIEW_ISSUES = '/viewissues'
REPORT_ISSUE = '/reportissue'
UPDATE_ISSUE = '/updateissue'
REPOSITORIES = '/repositories'

#
VIEW_ISSUES_PROMPT = 'Select the repository to view it\'s issues:'


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.route('/', methods=['GET'])
def root():
    return 'Just a random bot over here. o_o'


@app.route('/' + bot_token, methods=['POST', 'GET'])
def index():
    update = t.Update.de_json(request.get_json(), bot=t.Bot(bot_token))
    Logger.telegram_log(update.to_json())

    update._effective_user = User.init_with_telegram_user(update.effective_user)
    update.effective_user.save()

    msg = update.effective_message

    if msg.text in commands_methods.keys():
        # When user sends a new command assume it's a new conversation, end all previous
        conv = Conversation(update.effective_user, Command.by_name(msg.text))
        conv.end_previous_conversations()
        conv.save()
        return commands_methods.get(msg.text)(update)

    elif msg.text == VIEW_ISSUES_PROMPT:
        return view_issues(update, True)

    elif Conversation.active_with_user(update.effective_user) is not None:
        # If the user doesnt send a command but has an active conversation with shasbot

        conv = Conversation.active_with_user(update.effective_user)

        if conv.command.name == '/createissue':
            # The conversation is about creating an issue
            print('still talking about creating an issue')
            report_issue(update)

        else:
            print('We were having a conversation i don\'t remember')

        return ''

    else:
        return 'Command not found'


def start(update):
    update.message.reply_text('Hello! How can I help you?')
    return ''


def repositories(update):
    repos = BitbucketParser().parse_repositories(BitbucketClient().get_repositories())

    keyboard = []
    for repo in repos:
        if not repo.is_private:
            keyboard.append([t.InlineKeyboardButton(repo.name, url=repo.url)])

    update.message.reply_text(text='Shas Repositories:', reply_markup=t.InlineKeyboardMarkup(keyboard).to_json())
    return ''


def view_issues(update, is_callback=False):
    message = update.effective_message

    if message.text == VIEW_ISSUES:
        return send_repos(message, {"action": "view_issues"}, VIEW_ISSUES_PROMPT)

    elif is_callback:
        callback_dict = json.loads(update.callback_query.data)

        if callback_dict.get('action') == 'view_issues':
            repo_name = callback_dict.get('repository')
            issues = BitbucketParser.parse_issues(BitbucketClient().get_issues(repo_name))

            if issues is None:
                text = 'There are currently no issues for the {repo} repository.!'.format(repo=repo_name)
                return update.effective_message.edit_text(text=text).to_json()

            keyboard = []
            for issue in issues:
                keyboard.append([t.InlineKeyboardButton(issue.title, url=issue.url)])

            select_repo_text = 'Select an issue to view details:'

            return update.effective_message.edit_text(text=select_repo_text,
                                                      reply_markup=t.InlineKeyboardMarkup(keyboard).to_json()).to_json()

    return ''


def update_issues(udpate):
    pass


def send_repos(message, base_callback, text):
    if message is not None and base_callback is not None and base_callback is not {}:

        repos = BitbucketParser().parse_repositories(BitbucketClient().get_repositories())

        keyboard = []
        for repo in repos:
            if not repo.is_private:
                callback = dict(base_callback, **{'repository': repo.name})
                callback = json.dumps(callback)
                keyboard.append([t.InlineKeyboardButton(repo.name, callback_data=callback)])

        sent = message.reply_text(text=text,
                                  reply_markup=t.InlineKeyboardMarkup(keyboard).to_json())

        return sent.to_json()

    else:
        return Exception


def report_issue(update):
    message = update.effective_message

    if message.text == '/createissue':
        # First create an empty issue
        # issue = Issue(conversation=conversation)
        # issue.save()

        repos = BitbucketParser().parse_repositories(BitbucketClient().get_repositories())

        keyboard = []
        for repo in repos:
            if not repo.is_private:
                callback = json.dumps({"type": "issue", "repository": repo.name})
                keyboard.append([t.InlineKeyboardButton(repo.name, callback_data=callback)])

        select_repo_text = 'Select the repository to create the issue:'

        update.message.reply_text(text=select_repo_text, reply_markup=t.InlineKeyboardMarkup(keyboard).to_json())

        return ''
    # if step == 'title':
    #     pass  # TODO
    # if step == 'confirm':
    #     pass  # TODO
    else:
        return 'Invalid step to create issue: {}'.format(message.text)


commands_methods = {
    START: start,
    REPOSITORIES: repositories,
    VIEW_ISSUES: view_issues,
    UPDATE_ISSUE: update_issues,
    REPORT_ISSUE: report_issue,
}
