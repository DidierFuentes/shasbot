from datetime import datetime, timezone, timedelta

from sqlalchemy.orm import relationship

from db.database import db_session, Base

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Boolean

# noinspection PyPackageRequirements
from telegram import User as tUser


class Conversation(Base):
    __tablename__ = 'conversation'

    tz = timezone(-timedelta(hours=4))

    conversation_id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey('user.user_id'))
    command_id = Column(ForeignKey('command.command_id'))
    date_started = Column(DateTime, default=datetime.now(tz))
    last_message_date = Column(DateTime)
    finished = Column(Boolean, default=False)

    user = relationship('User', back_populates='conversations')
    command = relationship('Command', back_populates='conversations')
    issue = relationship('Issue', back_populates='conversation')

    def __init__(self, user, command):
        self.user_id = user.user_id
        self.command_id = command.command_id
        self.user = user
        self.command = command

    def __repr__(self):
        return '<Conversation(user={user}, command={command})>'.format(
            user=self.user.username, command=self.command.name)

    def save(self):
        if isinstance(self.conversation_id, Column) or self.conversation_id is None:
            self.last_message_date = datetime.now(Conversation.tz)
            db_session.add(self)
        db_session.commit()

    def end_previous_conversations(self):
        """ End all previous conversations with the same user. """
        Conversation.end_with_user(self.user)

    @staticmethod
    def end_with_user(user):
        """ Mark all previous conversation with given user as finished. """
        conv = Conversation.active_with_user(user)
        if conv is not None:
            conv.finished = True
            conv.save()

    @staticmethod
    def active_with_user(user):
        return db_session.query(Conversation) \
            .filter(Conversation.user_id == user.user_id) \
            .filter(Conversation.finished.is_(False)) \
            .first()


class User(tUser, Base):
    __tablename__ = 'user'

    user_id = Column(Integer, primary_key=True)
    username = Column(String(50))
    last_conversation = Column(DateTime)

    conversations = relationship('Conversation', back_populates='user')

    # noinspection PyShadowingBuiltins
    def __init__(self, id, first_name, last_name=None, username=None, language_code=None, bot=None,
                 **kwargs):

        # Telegram user init
        self.id = int(id)
        self.first_name = first_name

        # Optionals
        self.last_name = last_name
        self.username = username
        self.language_code = language_code

        self.bot = bot

        self._id_attrs = (self.id,)

        # Custom user init

        self.user_id = self.id

    def __repr__(self):
        return '<User(id={id}, username={username})>'.format(
            id=self.user_id, username=self.username)

    @classmethod
    def init_with_telegram_user(cls, t_user):
        temp = db_session.query(User).filter_by(user_id=t_user.id).one_or_none()

        if temp is None:
            return cls(t_user.id, t_user.first_name, last_name=t_user.last_name,
                       username=t_user.username, language_code=t_user.language_code, bot=t_user.bot)
        else:
            temp.first_name = t_user.first_name
            temp.last_name = t_user.last_name
            temp.username = t_user.username
            temp.language_code = t_user.language_code
            temp.bot = t_user.bot

            return temp

    @staticmethod
    def by_id(user_id):
        return db_session.query(User).filter(User.user_id == user_id).first()

    def save(self):
        if db_session.query(User).filter_by(username=self.username) is None:
            db_session.add(self)
        db_session.commit()


class Command(Base):
    __tablename__ = 'command'

    command_id = Column(Integer, primary_key=True)
    name = Column(String(25))
    description = Column(String(500))

    conversations = relationship('Conversation', back_populates='command')

    def __repr__(self):
        return '<Command(name=\'{name}\')>'.format(name=self.name)

    @staticmethod
    def by_name(name):
        return db_session.query(Command).filter(Command.name == name).first()


class Configuration(Base):
    __tablename__ = 'configuration'

    configuration_id = Column(Integer, primary_key=True)
    name = Column(String)
    value = Column(String)

    def __repr__(self):
        return '<Configuration(name={name})>'.format(name=self.name)

    @staticmethod
    def by_name(name):
        return db_session.query(Configuration).filter(Configuration.name == name).one_or_none()
