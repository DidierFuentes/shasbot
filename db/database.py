import json

from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

Base = declarative_base()


def get_config(key):
    return json.loads(open('shasbot_credentials').read()).get(key)

connection_structure = 'mysql://{user}:{password}@{host}{port}/{database}'

connection_string = connection_structure.format(user=get_config('database_user'),
                                                password=get_config('database_password'),
                                                host=get_config('database_host'),
                                                port=get_config('database_port'),
                                                database=get_config('database_name')
                                                )

engine = create_engine(connection_string, convert_unicode=True)
metadata = MetaData()
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))


def init_db():
    metadata.create_all(bind=engine)
