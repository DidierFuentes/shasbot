import json

import certifi
import urllib3
import config_manager


class BitbucketClient:

    def __init__(self):
        self.base_url = 'https://api.bitbucket.org/2.0/'
        self.shas_team_id = config_manager.shas_team_id()
        self.didier_id = config_manager.bitbucket_user_id('didierfuentes')
        self.bitbucket_token = config_manager.bitbucket_token()
        self.headers = {
            'Authorization': 'Basic {token}'.format(token=self.bitbucket_token)
        }

    def get_repositories(self):
        url = '{base}teams/{user_id}/repositories'.format(base=self.base_url, user_id=self.didier_id)

        return self.make_get_request(url, headers=self.headers)

    def get_issues(self, repository):

        if isinstance(repository, str):
            repo_id = config_manager.repository_id(repository)
            url = '{base}repositories/%7B%7D/{repo_id}/issues'.format(base=self.base_url, repo_id=repo_id)

            return self.make_get_request(url, headers=self.headers)

        else:
            return []

    def create_issue(self, issue):
        repo_id = config_manager.repository_id(issue.repository)
        url = '{base}repositories/%7B%7D/{repo_id}/issues'.format(base=self.base_url, repo_id=repo_id)

        return self.make_post_request(url, headers=self.headers, params=issue.as_dict())

    def make_get_request(self, url, headers=None):
        http = self.pool_manager()
        r = http.request('GET', url, headers=headers)
        return r.data

    def make_post_request(self, url, params=None, headers=None):
        if headers is None:
            headers = {}

        r = self.pool_manager().request(
            'POST',
            url,
            body=json.dumps(params),
            headers=dict({'Content-Type': 'application/json'}, **headers)
        )
        return r.data

    @staticmethod
    def pool_manager():
        return urllib3.PoolManager(
                                    cert_reqs='CERT_REQUIRED',
                                    ca_certs=certifi.where()
                                    )
