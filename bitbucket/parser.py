import json

from bitbucket.models import Repository, Issue


class BitbucketParser:
    @staticmethod
    def parse_repositories(data):

        data = json.loads(data.decode('utf-8'))
        result = []
        for repo in data.get('values'):
            repo = BitbucketParser.parse_repository(repo)
            repo.save()
            result.append(repo)

        return result

    @staticmethod
    def parse_repository(data):
        return Repository(
            data.get('name'),
            data.get('full_name'),
            data.get('has_issues'),
            data.get('uuid'),
            url=data.get('links').get('html').get('href'),
            is_private=data.get('is_private')
        )

    @staticmethod
    def parse_issues(data):
        data = json.loads(data.decode('utf-8'))
        issues = data.get('values')

        if issues is None:
            return None

        result = []
        for issue in issues:
            result.append(BitbucketParser.parse_issue(issue))

        return result

    @staticmethod
    def parse_issue(issue):
        return Issue(
            issue_id=issue.get('id'),
            title=issue.get('title'),
            content=issue.get('content').get('raw'),
            date=issue.get('created_on'),
            # TODO: parse kind and priority.
            kind=issue.get(''),
            priority=issue.get(''),
            repository=issue.get('repository').get('name'),
            state=issue.get('state'),
            url=issue.get('links').get('html').get('href')
        )
