"""
    This file includes bitbucket models.
"""
from sqlalchemy.orm import relationship

from db.database import Base, db_session
from db.models import Conversation
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey


class Repository(Base):
    __tablename__ = 'repository'

    repository_id = Column(Integer, primary_key=True)
    name = Column(String(50))
    uuid = Column(String(50))

    def __init__(self, name, full_name, has_issues, uuid, url='', is_private=False):
        self.name = name
        self.full_name = full_name
        self.has_issues = has_issues
        self.url = url
        self.is_private = is_private
        self.uuid = uuid

    def __repr__(self):
        return '<Repository(name={name})>'.format(name=self.name)

    @staticmethod
    def by_name(name):
        return db_session.query(Repository).filter(Repository.name == name).one()

    def save(self):
        if db_session.query(Repository).filter_by(uuid=self.uuid).one_or_none() is None:
            db_session.add(self)
        db_session.commit()


class Issue(Base):
    __tablename__ = 'issue'

    issue_id = Column(Integer, primary_key=True)
    title = Column(String(50))
    content = Column(String(1000))
    date = Column(DateTime)
    kind_id = Column(ForeignKey('issue_kind.issue_kind_id'))
    priority_id = Column(ForeignKey('issue_priority.issue_priority_id'))
    conversation_id = Column(ForeignKey('conversation.conversation_id'))
    repository = Column(String(50))
    state = Column(String(20), default='new')
    url = Column(String)

    kind = relationship('IssueKind', back_populates='issues')
    priority = relationship('IssuePriority', back_populates='issues')
    conversation = relationship('Conversation', back_populates='issue')

    def as_dict(self):
        result = {
            'title': self.title,
            'content': {
                'raw': self.content
            },
            'kind': self.kind.description,
            'priority': self.priority.description
        }
        if self.issue_id != '':
            result['id'] = self.issue_id
        if self.state != '':
            result['state'] = self.state
        if self.repository != '':
            result['repository'] = {
                'name': self.repository
            }

        return result

    def save(self):
        if isinstance(self.issue_id, Column) or self.issue_id is None:
            db_session.add(self)
        db_session.commit()


class IssueKind(Base):
    __tablename__ = 'issue_kind'

    issue_kind_id = Column(Integer, primary_key=True)
    description = Column(String(200))

    issues = relationship('Issue', back_populates='kind')


class IssuePriority(Base):
    __tablename__ = 'issue_priority'

    issue_priority_id = Column(Integer, primary_key=True)
    description = Column(String(200))

    issues = relationship('Issue', back_populates='priority')
