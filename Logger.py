from datetime import datetime


def log(text, file='shaslog'):
    f = open(file, 'a')
    f.write('\n---------- ' + datetime.now().strftime('%Y-%m-%d %H:%M:%S:%f %z') + '----------\n')
    f.write(str(text))


def telegram_log(text):
    log(text, 'telegram.log')


def bitbucket_log(text):
    log(text, 'bitbucket.log')
